#include "graphics.h"
#include "ebentoak.h"
#include "soinua.h"
#include "text.h"
#include "imagen.h"
#include <stdio.h>


typedef struct S_GURE_GAUZAK
{
    int idIrudi;
    int idIrudih;
    
}GURE_GAUZAK;

GURE_GAUZAK gureGauzak;


int hasieratu(void);

int main(int argc, char* str[])
{
    int ebentu = 0, irten = 0;
    POSIZIOA pos;

    hasieratu();
    while (!irten)
    {
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            pos = saguarenPosizioa();
            if ((pos.x > 300) && (pos.x < 300 + 40) && (pos.y > 200) && (pos.y < 200 + 38)) irten = 1;
        }
        if (ebentu == TECLA_ESCAPE) irten = 1;
    }

    sgItxi();
    return 0;
}


int hasieratu(void)
{
    int i;
    char str[128];
    double d = 7.3;

    if (sgHasieratu() == -1)
    {
        fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());
        return 0;
    }


    textuaGaitu();

    pantailaGarbitu();

    gureGauzak.idIrudi = irudiaKargatu(".\\img\\cerebro+a.bmp");
    gureGauzak.idIrudih = irudiaKargatu(".\\img\\hasitxikir.bmp");
    

    irudiaMugitu(gureGauzak.idIrudi, 0, 0);
    irudiakMarraztu();


    irudiaMugitu(gureGauzak.idIrudih, 30, 410);
    irudiakMarraztu();

    

    

    pantailaBerriztu();
    ;


    return 0;
}