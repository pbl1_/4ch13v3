#include "game02.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>

#define ONGI_ETORRI_MEZUA "ACHIEVE"
#define JOKOA_SOUND ".\\sound\\132TRANCE_02.wav"
#define JOKOA_PLAYER_IMAGE ".\\img\\invader.bmp"
#define JOKOA_MENU ".\\img\\menu.bmp"
#define JOKOA_SOUND_WIN ".\\sound\\BugleCall.wav"
#define JOKOA_SOUND_LOOSE ".\\sound\\terminator.wav" 
#define BUKAERA_SOUND_1 ".\\sound\\128NIGHT_01.wav"
#define BUKAERA_IMAGE ".\\img\\gameOver_1.bmp"

void sarreraMezuaIdatzi();
int aukeratu();
typedef struct S_GURE_GAUZAK
{
    int idIrudi, idMenu, idMapa, idAzalpenak, idKredituak;

}GURE_GAUZAK;

GURE_GAUZAK gureGauzak;
//int  BUKAERA_menua(EGOERA egoera);
int BUKAERA_irudiaBistaratu();


void jokoaAurkeztu(void)
{
    int ebentu = 0;
    char str[128];
    sarreraMezuaIdatzi();
    textuaIdatzi(250, 30, ONGI_ETORRI_MEZUA);
    do
    {
        ebentu = ebentuaJasoGertatuBada();
    } while (ebentu != TECLA_RETURN);
    pantailaGarbitu();
    pantailaBerriztu();
    aukeratu();
   
}

void sarreraMezuaIdatzi()
{
    pantailaGarbitu();
    gureGauzak.idIrudi = irudiaKargatu(".\\img\\cerebrordenador.bmp");
    irudiaMugitu(gureGauzak.idIrudi, 0, 0);
    irudiakMarraztu();
    textuaIdatzi(250, 30, ONGI_ETORRI_MEZUA);
    pantailaBerriztu();
}

int aukeratu()
{
    char str[128];
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idMenu = irudiaKargatu(JOKOA_MENU);
    irudiaMugitu(gureGauzak.idMenu, 10, 0);
    irudiakMarraztu();
    textuNormala();
    textuaIdatzi(200, 200, "Hasi!");
    textuaIdatzi(200, 300, "Azalpenak");
    textuaIdatzi(200, 400, "Kredituak");
    //textuaIdatzi(10, 100, str);
    pantailaBerriztu();
    ;
}

EGOERA jokatu(void)
{
    EGOERA  egoera = JOLASTEN;
    int ebentu = 0;
    
  
    audioInit();
    loadTheMusic(JOKOA_SOUND);
    playMusic();
    do
    {
        Sleep(2);
        pantailaGarbitu();
       
        irudiakMarraztu();
        pantailaBerriztu();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == TECLA_LEFT)
        {
            gureGauzak.idMapa = irudiaKargatu(".\\img\\Koloreak.bmp");
            irudiaMugitu(gureGauzak.idMapa, 20, 20);
            irudiakMarraztu();
        }
        if (ebentu== TECLA_DOWN)
        {
            gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\Letrak.bmp");
            irudiaMugitu(gureGauzak.idAzalpenak, 20, 20);
            irudiakMarraztu();
        }
        if (ebentu== TECLA_RIGHT)
        {
            gureGauzak.idKredituak = irudiaKargatu(".\\img\\Hutsuneak.bmp");
            irudiaMugitu(gureGauzak.idKredituak, 20, 20);
            irudiakMarraztu();
        }
        
    } while (egoera == JOLASTEN);
    //irudiaKendu(jokalaria.id);
    toggleMusic();
    audioTerminate();
    pantailaGarbitu();
    pantailaBerriztu();
    return egoera;
}


int  jokoAmaierakoa(EGOERA egoera)
{
    int ebentu = 0, id;
    int idAudioGame;

    loadTheMusic(BUKAERA_SOUND_1);
    if (egoera == IRABAZI)
    {
        idAudioGame = loadSound(JOKOA_SOUND_WIN);
        playSound(idAudioGame);
    }
    else
    {
        idAudioGame = loadSound(JOKOA_SOUND_LOOSE);
        playSound(idAudioGame);
    }
    id = BUKAERA_irudiaBistaratu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
    } while ((ebentu != TECLA_RETURN) && (ebentu != SAGU_BOTOIA_ESKUMA));
    audioTerminate();
    irudiaKendu(id);
    return (ebentu != TECLA_RETURN) ? 1 : 0;
}

int BUKAERA_irudiaBistaratu()
{
    int id = -1;
    id = irudiaKargatu(BUKAERA_IMAGE);
    irudiaMugitu(id, 200, 200);
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();
    return id;
}


